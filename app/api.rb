require 'rom'
require 'rom-sql'
require_relative 'api/artist_app'
require_relative 'api/album_app'
require_relative 'api/song_app'
require_relative 'api/user_app'
require_relative '../db/db'
require_relative '../lib/ext_hash'

module API
  class App < Sinatra::Base
    configure do
      DB.setup
    end

    use API::ArtistApp
    use API::AlbumApp
    use API::SongApp
    use API::UserApp
  end
end
